--Create 7/14/2017
--FJ Caballero


--crear y asignar privilegios a user_login
--Solo puede consultar registros sobre la tabla user
CREATE USER 'user_login'@'%' IDENTIFIED WITH mysql_native_password AS '80ca172801557aa70c54acece36d2df98dc5bb669adcdf166ea5e5fab1f46fb4';
GRANT SELECT ON *.* TO 'user_login'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT SELECT ON `hackatonge`.`user` TO 'user_login'@'%';

--crear y asignar privilegios a user_get
--Solo puede consultar registros sobre todas las tablas
CREATE USER 'user_get'@'%' IDENTIFIED WITH mysql_native_password AS 'dcbfb38cc1e31060bfa4b6c3bb6634e99e2df27b167c51c44b5c72e5e748b7ea';
GRANT SELECT ON *.* TO 'user_get'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT SELECT ON `hackatonge`.* TO 'user_get'@'%';

--crear y asignar privilegios a user_get
--Solo puede borrar registros sobre todas las tablas
CREATE USER 'user_delete'@'%' IDENTIFIED WITH mysql_native_password AS '982a651a7b474682198f00ea51ed8564b32065bc2762205f06203000d940945a';
GRANT USAGE ON *.* TO 'user_delete'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT DELETE ON `hackatonge`.* TO 'user_delete'@'%';

--crear y asignar privilegios a user_get
--Solo puede insertar y actulizar en las tablas
CREATE USER 'user_put'@'%' IDENTIFIED WITH mysql_native_password AS '86043f92213329353a374e7b4fe7a59906df34a8e6d38dda417045ef581ee3b3';
GRANT USAGE ON *.* TO 'user_put'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT INSERT, UPDATE ON `hackatonge`.* TO 'user_put'@'%';