package util.gewrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.request.TurbineRequest;

public class GeWrapper {

    private String pathExe;
    private TurbineRequest turbine;
    private ArrayList argsList;
    private ProcessBuilder pb;
    private ArrayList<String> result;

    public GeWrapper(String pathExe, TurbineRequest turbine) throws Exception {
        this.result = null;
        if (null == pathExe || 0 == pathExe.length()) {
            throw new Exception("Missing Path");
        }
        this.pathExe = pathExe;

        setArgs(turbine);
        prepareProcessBuilder();
    }

    protected void prepareProcessBuilder() {
        pb = new ProcessBuilder(this.argsList);
    }

    public ArrayList<String> run() throws Exception {
        ArrayList<String> res = new ArrayList<>();

        Process process = null;
        try {
            process = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(GeWrapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Process create error");
        }
        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        try {
            while ((line = br.readLine()) != null) {    
                res.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(GeWrapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Process outpupt reading error");
        }
        try {
            int exitValue = process.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(GeWrapper.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Process exit error");
        }
        this.result = res;
        return res;
    }

    public ArrayList<String> getLastResult() {
        return result;
    }

    public ArrayList<String> getResult() {
        return result;
    }

    public String getPathExe() {
        return pathExe;
    }

    public TurbineRequest getArgs() {
        return turbine;
    }

    public void setArgs(TurbineRequest turbine) throws Exception {
        if (turbine == null) {
            throw new Exception("Missing arguments");
        }
        this.turbine = turbine;

        argsList = new ArrayList();
        argsList.add(0, pathExe);
        argsList.add(turbine.getAtmosphere_p0_CMD());
        argsList.add(turbine.getAtmosphere_t0_CMD());
        argsList.add(turbine.getComb_chamber_Q_n_CMD());
        argsList.add(turbine.getComb_chamber_T_stag_out_CMD());
        argsList.add(turbine.getComb_chamber_eta_burn_CMD());
        argsList.add(turbine.getComp_eta_stag_p_CMD());
        argsList.add(turbine.getComp_pi_c_CMD());
        argsList.add(turbine.getInlet_sigma_CMD());
        argsList.add(turbine.getLoad_power_CMD());
        argsList.add(turbine.getOutlet_sigma_CMD());
        argsList.add(turbine.getRegenerator_T_stag_hot_in_CMD());
        argsList.add(turbine.getRegenerator_T_stag_hot_out_CMD());
        argsList.add(turbine.getTurbine_eta_stag_p_CMD());
        argsList.add(turbine.getTurbine_p_stag_out_CMD());
    }

}
