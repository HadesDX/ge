package util.result;

public class TurbineResult {

    private Double t_stage_out;
    private Double p_stage_out;

    public Double getT_stage_out() {
        return t_stage_out;
    }

    public void setT_stage_out(Double t_stage_out) {
        this.t_stage_out = t_stage_out;
    }

    public Double getP_stage_out() {
        return p_stage_out;
    }

    public void setP_stage_out(Double p_stage_out) {
        this.p_stage_out = p_stage_out;
    }

}
