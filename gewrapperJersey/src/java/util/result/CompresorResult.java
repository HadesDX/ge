package util.result;

public class CompresorResult {

    private Double t_stag_out;
    private Double p_stag_out;

    public Double getT_stag_out() {
        return t_stag_out;
    }

    public void setT_stag_out(Double t_stag_out) {
        this.t_stag_out = t_stag_out;
    }

    public Double getP_stag_out() {
        return p_stag_out;
    }

    public void setP_stag_out(Double p_stag_out) {
        this.p_stag_out = p_stag_out;
    }
}
