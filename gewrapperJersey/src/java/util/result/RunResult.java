package util.result;

public class RunResult {

    private Integer requestIndex;
    private Double load_g_aire;
    private CompresorResult compresor;
    private TurbineResult turbine;

    public RunResult() {
        compresor = new CompresorResult();
        turbine = new TurbineResult();
    }

    public Double getLoad_g_aire() {
        return load_g_aire;
    }

    public void setLoad_g_aire(Double load_g_aire) {
        this.load_g_aire = load_g_aire;
    }

    public CompresorResult getCompresor() {
        return compresor;
    }

    public void setCompresor(CompresorResult compresor) {
        this.compresor = compresor;
    }

    public TurbineResult getTurbine() {
        return turbine;
    }

    public void setTurbine(TurbineResult turbine) {
        this.turbine = turbine;
    }

    public Integer getRequestIndex() {
        return requestIndex;
    }

    public void setRequestIndex(Integer requestIndex) {
        this.requestIndex = requestIndex;
    }

}
