package util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class SHA256 {

    public SHA256() {
    }

    public String HASH(String c1) throws Exception {
        if (c1 == null) {
            return null;
        }
        MessageDigest md;
        byte[] digest;
        String output;
        BigInteger bigInt;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(c1.getBytes("UTF-8")); // Change this to "UTF-16" if needed
        } catch (Exception ex) {
            throw new Exception("Could'n process SHA256");
        }

        digest = md.digest();
        bigInt = new BigInteger(1, digest);
        output = bigInt.toString(16);
        while (output.length() < 64) {
            output = "0" + output;
        }
        return output;
    }
}
