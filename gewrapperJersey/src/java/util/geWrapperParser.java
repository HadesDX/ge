package util;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.result.RunResult;

public class geWrapperParser {

    ArrayList<String> original;

    public geWrapperParser(ArrayList<String> original) {
        this.original = original;
    }

    public RunResult parse() {
        if (original == null || original.size() < 5) {
            return null;
        }
        RunResult r = new RunResult();
        try {
            r.setLoad_g_aire(Double.parseDouble(original.get(0).substring(11, original.get(0).length())));
            r.getCompresor().setT_stag_out(Double.parseDouble(original.get(1).substring(16, original.get(1).length())));
            r.getCompresor().setP_stag_out(Double.parseDouble(original.get(2).substring(16, original.get(2).length())));
            r.getTurbine().setT_stage_out(Double.parseDouble(original.get(3).substring(20, original.get(3).length())));
            r.getTurbine().setP_stage_out(Double.parseDouble(original.get(4).substring(20, original.get(4).length())));
        } catch (NumberFormatException ex) {
            Logger.getLogger(GsonMessageBodyHandler.class.getName()).log(Level.SEVERE, "Double parse error", ex);
            throw ex;
        }
        return r;
    }
}
