package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author diego
 */
public class LocalEntityManager {

    protected static EntityManagerFactory factory;
    @PersistenceContext(unitName = "eripPU")
    protected static EntityManager em;

    protected LocalEntityManager() {
    }

    public synchronized static final EntityManager getEntityManager() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("gewrapperJerseyPU");
            if (em == null) {
                em = factory.createEntityManager();
            }
        }
        return em;
    }

    public synchronized static final EntityManagerFactory getEntityManagerFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("eripPU");
        }
        return factory;
    }
}
