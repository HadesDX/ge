package util.request;

public class TurbineRequest {

    private Integer requestIndex;
    private Double atmosphere_p0;
    private Double atmosphere_t0;
    private Double inlet_sigma;
    private Double comp_pi_c;
    private Double comp_eta_stag_p;
    private Double comb_chamber_eta_burn;
    private Double comb_chamber_Q_n;
    private Double comb_chamber_T_stag_out;
    private Double turbine_eta_stag_p;
    private Double turbine_p_stag_out;
    private Double load_power;
    private Double outlet_sigma;
    private Double regenerator_T_stag_hot_in;
    private Double regenerator_T_stag_hot_out;

    public void setRequestIndex(Integer requestIndex) {
        this.requestIndex = requestIndex;
    }

    public void setAtmosphere_p0(Double atmosphere_p0) {
        this.atmosphere_p0 = atmosphere_p0;
    }

    public void setAtmosphere_t0(Double atmosphere_t0) {
        this.atmosphere_t0 = atmosphere_t0;
    }

    public void setInlet_sigma(Double inlet_sigma) {
        this.inlet_sigma = inlet_sigma;
    }

    public void setComp_pi_c(Double comp_pi_c) {
        this.comp_pi_c = comp_pi_c;
    }

    public void setComp_eta_stag_p(Double comp_eta_stag_p) {
        this.comp_eta_stag_p = comp_eta_stag_p;
    }

    public void setComb_chamber_eta_burn(Double comb_chamber_eta_burn) {
        this.comb_chamber_eta_burn = comb_chamber_eta_burn;
    }

    public void setComb_chamber_Q_n(Double comb_chamber_Q_n) {
        this.comb_chamber_Q_n = comb_chamber_Q_n;
    }

    public void setComb_chamber_T_stag_out(Double comb_chamber_T_stag_out) {
        this.comb_chamber_T_stag_out = comb_chamber_T_stag_out;
    }

    public void setTurbine_eta_stag_p(Double turbine_eta_stag_p) {
        this.turbine_eta_stag_p = turbine_eta_stag_p;
    }

    public void setTurbine_p_stag_out(Double turbine_p_stag_out) {
        this.turbine_p_stag_out = turbine_p_stag_out;
    }

    public void setLoad_power(Double load_power) {
        this.load_power = load_power;
    }

    public void setOutlet_sigma(Double outlet_sigma) {
        this.outlet_sigma = outlet_sigma;
    }

    public void setRegenerator_T_stag_hot_in(Double regenerator_T_stag_hot_in) {
        this.regenerator_T_stag_hot_in = regenerator_T_stag_hot_in;
    }

    public void setRegenerator_T_stag_hot_out(Double regenerator_T_stag_hot_out) {
        this.regenerator_T_stag_hot_out = regenerator_T_stag_hot_out;
    }

    public String getAtmosphere_p0_CMD() {
        return "--atmosphere-p0=" + atmosphere_p0;
    }

    public String getAtmosphere_t0_CMD() {
        return "--atmosphere-t0=" + atmosphere_t0;
    }

    public String getInlet_sigma_CMD() {
        return "--inlet-sigma=" + inlet_sigma;
    }

    public String getComp_pi_c_CMD() {
        return "--comp-pi_c=" + comp_pi_c;
    }

    public String getComp_eta_stag_p_CMD() {
        return "--comp-eta_stag_p=" + comp_eta_stag_p;
    }

    public String getComb_chamber_eta_burn_CMD() {
        return "--comb_chamber-eta_burn=" + comb_chamber_eta_burn;
    }

    public String getComb_chamber_Q_n_CMD() {
        return "--comb_chamber-Q_n=" + comb_chamber_Q_n;
    }

    public String getComb_chamber_T_stag_out_CMD() {
        return "--comb_chamber-T_stag_out=" + comb_chamber_T_stag_out;
    }

    public String getTurbine_eta_stag_p_CMD() {
        return "--turbine-eta_stag_p=" + turbine_eta_stag_p;
    }

    public String getTurbine_p_stag_out_CMD() {
        return "--turbine-p_stag_out=" + turbine_p_stag_out;
    }

    public String getLoad_power_CMD() {
        return "--load-power=" + load_power;
    }

    public String getOutlet_sigma_CMD() {
        return "--outlet-sigma=" + outlet_sigma;
    }

    public String getRegenerator_T_stag_hot_in_CMD() {
        return "--regenerator-T_stag_hot_in=" + regenerator_T_stag_hot_in;
    }

    public String getRegenerator_T_stag_hot_out_CMD() {
        return "--regenerator-T_stag_hot_out=" + regenerator_T_stag_hot_out;
    }

    public Integer getRequestIndex() {
        return requestIndex;
    }

    public Double getAtmosphere_p0() {
        return atmosphere_p0;
    }

    public Double getAtmosphere_t0() {
        return atmosphere_t0;
    }

    public Double getInlet_sigma() {
        return inlet_sigma;
    }

    public Double getComp_pi_c() {
        return comp_pi_c;
    }

    public Double getComp_eta_stag_p() {
        return comp_eta_stag_p;
    }

    public Double getComb_chamber_eta_burn() {
        return comb_chamber_eta_burn;
    }

    public Double getComb_chamber_Q_n() {
        return comb_chamber_Q_n;
    }

    public Double getComb_chamber_T_stag_out() {
        return comb_chamber_T_stag_out;
    }

    public Double getTurbine_eta_stag_p() {
        return turbine_eta_stag_p;
    }

    public Double getTurbine_p_stag_out() {
        return turbine_p_stag_out;
    }

    public Double getLoad_power() {
        return load_power;
    }

    public Double getOutlet_sigma() {
        return outlet_sigma;
    }

    public Double getRegenerator_T_stag_hot_in() {
        return regenerator_T_stag_hot_in;
    }

    public Double getRegenerator_T_stag_hot_out() {
        return regenerator_T_stag_hot_out;
    }

}
