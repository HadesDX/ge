package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "setting")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Setting.findAll", query = "SELECT s FROM Setting s")
    ,@NamedQuery(name = "Setting.findAllByUsID", query = "SELECT s FROM Setting s where s.userUsId= :userUsId")
    , @NamedQuery(name = "Setting.findBySeId", query = "SELECT s FROM Setting s WHERE s.seId = :seId")
    , @NamedQuery(name = "Setting.findBySeShow", query = "SELECT s FROM Setting s WHERE s.seShow = :seShow")})
public class Setting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "se_id")
    private Integer seId;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "se_value")
    private String seValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "se_show")
    private short seShow;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "user_us_id", referencedColumnName = "us_id")
    @ManyToOne(optional = false)
    private User userUsId;

    public Setting() {
    }

    public Setting(Integer seId) {
        this.seId = seId;
    }

    public Setting(Integer seId, String seValue, short seShow) {
        this.seId = seId;
        this.seValue = seValue;
        this.seShow = seShow;
    }

    public Integer getSeId() {
        return seId;
    }

    public void setSeId(Integer seId) {
        this.seId = seId;
    }

    public String getSeValue() {
        return seValue;
    }

    public void setSeValue(String seValue) {
        this.seValue = seValue;
    }

    public short getSeShow() {
        return seShow;
    }

    public void setSeShow(short seShow) {
        this.seShow = seShow;
    }

    public User getUserUsId() {
        return userUsId;
    }

    public void setUserUsId(User userUsId) {
        this.userUsId = userUsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seId != null ? seId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Setting)) {
            return false;
        }
        Setting other = (Setting) object;
        if ((this.seId == null && other.seId != null) || (this.seId != null && !this.seId.equals(other.seId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Setting[ seId=" + seId + " ]";
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
