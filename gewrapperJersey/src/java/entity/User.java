package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
    , @NamedQuery(name = "User.findByUsId", query = "SELECT u FROM User u WHERE u.usId = :usId")
    , @NamedQuery(name = "User.findByUsName", query = "SELECT u FROM User u WHERE u.usName = :usName")})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "us_id")
    private Integer usId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "us_name")
    private String usName;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "us_pass")
    private byte[] usPass;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUsId")
    private Collection<Setting> settingCollection;

    public User() {
    }

    public User(Integer usId) {
        this.usId = usId;
    }

    public User(Integer usId, String usName, byte[] usPass) {
        this.usId = usId;
        this.usName = usName;
        this.usPass = usPass;
    }

    public Integer getUsId() {
        return usId;
    }

    public void setUsId(Integer usId) {
        this.usId = usId;
    }

    public String getUsName() {
        return usName;
    }

    public void setUsName(String usName) {
        this.usName = usName;
    }

    public byte[] getUsPass() {
        return usPass;
    }

    public void setUsPass(byte[] usPass) {
        this.usPass = usPass;
    }

    @XmlTransient
    public Collection<Setting> getSettingCollection() {
        return settingCollection;
    }

    public void setSettingCollection(Collection<Setting> settingCollection) {
        this.settingCollection = settingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usId != null ? usId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.usId == null && other.usId != null) || (this.usId != null && !this.usId.equals(other.usId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.User[ usId=" + usId + " ]";
    }
    
}
