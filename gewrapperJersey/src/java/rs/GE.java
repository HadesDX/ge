package rs;

import exception.InternalServerError;
import util.gewrapper.GeWrapper;
import util.geWrapperParser;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import util.request.TurbineRequest;
import util.result.RunResult;

@Path("wrapper")
public class GE {

    @Context
    private UriInfo context;

    public GE() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<RunResult> postCMD(ArrayList<TurbineRequest> content) {

        try {
            ArrayList<RunResult> rr = new ArrayList<RunResult>();
            for (TurbineRequest t : content) {
                util.gewrapper.GeWrapper ge = new GeWrapper("D:\\Workspace\\GE\\turbine_lib\\turbine_lib.exe", t);
                RunResult r = new geWrapperParser(ge.run()).parse();
                if (r == null) {
                    r = new RunResult();
                } else {
                    r.setRequestIndex(t.getRequestIndex());
                }
                rr.add(r);
            }
            return rr;
        } catch (Exception ex) {
            Logger.getLogger(GE.class.getName()).log(Level.SEVERE, null, ex);
            throw new InternalServerError("Failed to process wrapper");
        }
    }
}
