package rs;

import entity.User;
import exception.BadRequest;
import exception.InternalServerError;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import util.LocalEntityManager;
import util.SHA256;
import util.SimpleUser;

@Path("user")
public class UserFacadeREST extends AbstractFacade<User> {

    @PersistenceContext(unitName = "gewrapperJerseyPU")
    private EntityManager em;

    public UserFacadeREST() {
        super(User.class);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(SimpleUser us) {
        if (us.getName() == null || us.getName().length() < 1) {
            throw new BadRequest("Name missing");
        }
        if (us.getPass() == null) {
            throw new BadRequest("Password missing");
        }
        if (us.getPass().length() < 6) {
            throw new BadRequest("Insecure password, lenght must be at least 6");
        }
        User entity = new User();
        entity.setUsName(us.getName());
        try {
            entity.setUsPass(new SHA256().HASH(us.getPass()).getBytes("utf8"));
        } catch (Exception ex) {
            Logger.getLogger(UserFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            throw new InternalServerError("Can't create user");
        }
        super.create(entity);
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public SimpleUser login(SimpleUser us) {
        if (us.getName() == null || us.getName().length() < 1) {
            throw new BadRequest("Name missing");
        }
        if (us.getPass() == null) {
            throw new BadRequest("Password missing");
        }

        Query q = getEntityManager().createNamedQuery("User.findByUsName");
        q.setParameter("usName", us.getName().trim());
        User u = null;
        try {
            u = (User) q.getSingleResult();
        } catch (Exception ex) {
            throw new BadRequest("User dont exist");
        }

        try {
            byte[] b = new byte[255];
            byte[] src = new SHA256().HASH(us.getPass()).getBytes("utf8");
            System.arraycopy(src, 0, b, 0, src.length);
            if (Arrays.equals(b, u.getUsPass())) {
                us.setId(u.getUsId());
            } else {
                throw new BadRequest("User login failed");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            throw new InternalServerError("Can't verify password");
        }
        us.setPass(null);
        return us;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, User entity) {
        throw new BadRequest("Not implemeted");
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = LocalEntityManager.getEntityManager();
        }
        return em;
    }

    @Override
    protected EntityManagerFactory getEntityManagerFactory() {
        return LocalEntityManager.getEntityManagerFactory();
    }

}
