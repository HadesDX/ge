package rs;

import entity.Setting;
import entity.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import util.LocalEntityManager;

@Path("setting")
public class SettingFacadeREST extends AbstractFacade<Setting> {

    @PersistenceContext(unitName = "gewrapperJerseyPU")
    private EntityManager em;

    public SettingFacadeREST() {
        super(Setting.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Setting entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}/mark")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") String id, Setting entity) {
        //super.edit(entity);
    }

    /*@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Setting find(@PathParam("id") String id) {
        return super.find(id);
    }*/
    @POST
    @Path("user/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void saveAll(List<Setting> set) {
        List<Setting> r = new ArrayList<Setting>();
        if (set != null) {
            Date d = new Date();
            for (Setting s : set) {
                s.setDate(d);
                s.setUserUsId(new UserFacadeREST().find(s.getUserUsId().getUsId()));
                create(s);
                r.add(s);
            }
        }
        //return r;
    }

    @GET
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Setting> findAll(@PathParam("id") Integer id) {
        User u = getEntityManager().find(User.class, id);
        if (u != null) {
            Query query = getEntityManager().createNamedQuery("Setting.findAllByUsID");
            query.setParameter("userUsId", u);
            ArrayList<Setting> r = new ArrayList<>(query.getResultList());
            for (Setting s : r) {
                getEntityManager().detach(s.getUserUsId());
                getEntityManager().detach(s);
                s.getUserUsId().setUsPass(null);
            }
            return r;
        }
        return new ArrayList<>();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Setting> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }*/
    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = LocalEntityManager.getEntityManager();
        }
        return em;
    }

    @Override
    protected EntityManagerFactory getEntityManagerFactory() {
        return LocalEntityManager.getEntityManagerFactory();
    }

}
