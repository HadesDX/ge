/* 
 Created on : Jul 14, 2017, 11:29:22 PM
 Author     : FJ Caballero
 */
var cont = 0;

var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(231,233,237)'
};

/***
 * Agrega una estructura de registro (Elemento html) al DOM
 * @returns {undefined}
 */
function addForm() {
    var content = "<div class=\"row idContent\"> <article><form> <div class=\"panel-primary panel\">  <div class=\"panel-heading panelInput\"> <h6 \" >Input <span class=\"glyphicon glyphicon-collapse-down\" data-toggle=\"collapse\" data-target=\"#inputData" + cont + "\" ></span><span id=\"btoDel\" onclick=\"delForm(this);\" class=\"glyphicon glyphicon-remove\"></span></h6> </div> <div id=\"inputData" + cont + "\" class=\"panel-body collapse\">";
    content += $("#inputData").html();
    content += "</div> </div></form> </article> </div>";
    var d = $(content);
    $("#inputContent").append(d);
    $("#inputData input").each(function () {
        d.find('[name="' + $(this).attr('name') + '"]').val($(this).val());
    });
    cont++;
    //console.log("adjuntado");
}
function addFormSettig(setting) {
    var content = "<div class=\"row idContent\"> <article><form> <div class=\"panel-primary panel\">  <div class=\"panel-heading panelInput\"> <h6 \" >Input <span class=\"glyphicon glyphicon-collapse-down\" data-toggle=\"collapse\" data-target=\"#inputData" + cont + "\" ></span><span id=\"btoDel\" onclick=\"delForm(this);\" class=\"glyphicon glyphicon-remove\"></span></h6> </div> <div id=\"inputData" + cont + "\" class=\"panel-body collapse\">";
    content += $("#inputData").html();
    content += "</div> </div></form> </article> </div>";
    var d = $(content);
    $("#inputContent").append(d);
    var o = JSON.parse(setting);
    for (var nam in o) {
        d.find('[name="' + nam + '"]').val(o[nam]);
    }
    cont++;
}

/*
 * Borra una estrucctura de registro (tagc html)cdel DOM
 * @param {type} tag Elemento del que se busca a su padre idConten para eliminarlo
 * @returns {undefined}
 */
function delForm(tag) {
    $(tag).closest(".idContent").remove();
}

function extractSet(el) {
    var o = {};
    el.each(function () {
        var n = $(this).attr('name');
        if (n) {
            o[n] = $(this).val();
        }
    });
    return o;
}

function serializeJson(form) {
    var ar = new Array();
    form.each(function () {
        ar.push(extractSet($(this).find('input')));
    });
    return JSON.stringify(ar);
}
function sendListSettingsUser() {
    var v = new Array();
    $('#panelParameters form').each(function () {
        v.push({seValue: JSON.stringify(extractSet($(this).find('input'))), userUsId: {usId: user.id}});
    });
    v = JSON.stringify(v);
    $('#save span').removeClass('glyphicon-floppy-disk');
    $('#save span').addClass('glyphicon-floppy-open');
    $.ajax({
        url: 'ws/setting/user/' + user.id,
        type: 'POST',
        contentType: 'application/json',
        data: v
    }).done(function (data) {
        $('#save span').removeClass('glyphicon-floppy-open');
        $('#save span').addClass('glyphicon-floppy-saved');
        $('#save span').animate({
            opacity: 1
        }, 2000, function () {
            $('#save span').addClass('glyphicon-floppy-disk');
            $('#save span').removeClass('glyphicon-floppy-saved');
        });
    }).fail(function (a, b, c) {
        $('#save span').removeClass('glyphicon-floppy-open');
        $('#save span').addClass('glyphicon-floppy-remove');
        $('#save span').animate({
            opacity: 1
        }, 2000, function () {
            $('#save span').addClass('glyphicon-floppy-disk');
            $('#save span').removeClass('glyphicon-floppy-remove');
        });
    });
}
function getAllSettingsUser() {
    $.ajax({
        url: 'ws/setting/user/' + user.id,
        type: 'GET',
        contentType: 'application/json',
        cache: false
    }).done(function (data) {
        var table = $('#logSec tbody');
        table.empty();
        for (var i in data) {
            data[i].dateTime = new Date(data[i].date).getTime();
            var tr = $('<tr>').attr('data-value', data[i].seValue);
            tr.append($('<td>').text(data[i].seId));
            tr.append($('<td>').text(data[i].date));
            //tr.append($('<td>').text(data[i].seValue.replace(',', ',\n')));
            tr.append($('<td>').text());
            tr.append($('<td><span class="glyphicon glyphicon-share btn"></span></td>').on('click', function () {
                addFormSettig($(this).closest('tr').attr('data-value'));
            }));
            //  tr.append($('<td><span class="glyphicon glyphicon-heart-empty btn"></span></td>'));
            table.append(tr);
        }
        console.log(data);
    }).fail(function (a, b, c) {
        console.log(a, b, c);
    });
}

function sendSet(event) {
    $.ajax({
        url: 'ws/wrapper/',
        type: 'POST',
        contentType: 'application/json',
        data: serializeJson($('#panelParameters form'))
    }).done(function (data) {
        console.log(data);
        fillTable(data);
        if (chartAir) {
            chartAir.destroy();
        }
        if (chartCompresor) {
            chartCompresor.destroy();
        }
        if (chartTurbine) {
            chartTurbine.destroy();
        }
        if (chartCompresorP) {
            chartCompresorP.destroy();
        }
        if (chartTurbineP) {
            chartTurbineP.destroy();
        }
        if (chartTurbineCompresor) {
            chartTurbineCompresor.destroy();
        }
        if (chartTurbineCompresorP) {
            chartTurbineCompresorP.destroy();
        }
        chart($('#chartAir'), buildDataCharAir(data));
        chart($('#chartCompresor'), buildDataCharCompresor(data));
        chart($('#chartTurbine'), buildDataCharTurbine(data));
        chart($('#chartCompresorp'), buildDataCharCompresorP(data));
        chart($('#chartTurbinep'), buildDataCharTurbineP(data));
        chart($('#chartTurbineCompresor'), buildDataCharTurbineCompresor(data));
        chart($('#chartTurbineCompresorP'), buildDataCharTurbineCompresorP(data));
        $('section').removeClass('hidden');
    }).fail(function (a, b, c) {
        console.log(a, b, c);
    });
}

function fillTable(data) {
    var t = $('#tableData');
    var b = t.find('tbody').empty();
    for (var i in data) {
        var tr = $('<tr>');
        tr.append($('<th>').text(i));
        tr.append($('<td>').text(data[i].load_g_aire));
        tr.append($('<td>').text(data[i].compresor.p_stag_out));
        tr.append($('<td>').text(data[i].compresor.t_stag_out));
        tr.append($('<td>').text(data[i].turbine.t_stage_out));
        tr.append($('<td>').text(data[i].turbine.p_stage_out));
        b.append(tr);
    }
}
var chartAir;
var chartCompresor;
var chartTurbine;
var chartCompresorP;
var chartTurbineP;
var chartTurbineCompresor;
var chartTurbineCompresorP;

function buildDataCharAir(data) {
    var load = {
        label: 'Load Air',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };
    var set = {
        labels: new Array(),
        datasets: new Array(load)
    };

    for (var i in data) {
        set.labels.push(i);
        load.data.push(data[i].load_g_aire);
    }
    return set;
}
function buildDataCharCompresor(data) {
    var c1 = {
        label: 'Air mass flow vs Compresor Temp',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1)
    };

    for (var i in data) {
        set.labels.push(data[i].compresor.t_stag_out);
        c1.data.push(data[i].load_g_aire);
    }

    return set;
}
function buildDataCharTurbine(data) {
    var c1 = {
        label: 'Air mass flow vs Turbine Temp',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1)
    };

    for (var i in data) {
        set.labels.push(data[i].turbine.t_stage_out);
        c1.data.push(data[i].load_g_aire);
    }

    return set;
}
function buildDataCharCompresorP(data) {
    var c1 = {
        label: 'Air mass flow vs Compresor Pressure',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1)
    };

    for (var i in data) {
        set.labels.push(data[i].compresor.p_stag_out);
        c1.data.push(data[i].load_g_aire);
    }

    return set;
}
function buildDataCharTurbineP(data) {
    var c1 = {
        label: 'Air mass flow vs Turbine Pressure',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1)
    };

    for (var i in data) {
        set.labels.push(data[i].turbine.p_stage_out);
        c1.data.push(data[i].load_g_aire);
    }

    return set;
}
function buildDataCharTurbineCompresor(data) {
    var c1 = {
        label: 'Air mass flow vs Compresor Temp',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };
    var c2 = {
        label: 'Air mass flow vs Turbine Temp',
        //backgroundColor: chartColors.red,
        borderColor: chartColors.red,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1, c2)
    };

    for (var i in data) {
        set.labels.push(data[i].load_g_aire);
        c1.data.push(data[i].compresor.t_stag_out);
        c2.data.push(data[i].turbine.t_stage_out);
    }

    return set;
}
function buildDataCharTurbineCompresorP(data) {
    var c1 = {
        label: 'Air mass flow vs Compresor Pressure',
        //backgroundColor: chartColors.blue,
        borderColor: chartColors.blue,
        data: new Array()
    };
    var c2 = {
        label: 'Air mass flow vs Turbine Pressure',
        //backgroundColor: chartColors.red,
        borderColor: chartColors.red,
        data: new Array()
    };

    var set = {
        labels: new Array(),
        datasets: new Array(c1, c2)
    };

    for (var i in data) {
        set.labels.push(data[i].load_g_aire);
        c1.data.push(data[i].compresor.p_stag_out);
        c2.data.push(data[i].turbine.p_stage_out);
    }

    return set;
}
function chart(ele, data) {
    var ctx = ele;
    //myChart.destroy();
    var myChart = new Chart(ctx, {
        type: 'line',
        data: data
    });
}

Chart.defaults.global.defaultColor = 'rgba(100, 100, 100, 0.1)';

function downloadResult() {
    var out = "";
    var table = $('#tableData');
    table.find('thead').find('th').each(function () {
        return out += $(this).text() + ",";
    });
    out = out.substr(0, out.length - 1);
    out += "\n";

    table.find('tbody').find('tr').each(function () {
        $(this).find('th,td').each(function () {
            return out += $(this).text() + ",";
        });
        out = out.substr(0, out.length - 1);
        out += "\n";
    });
    console.log(out);
    var file = new File([out], "resultTurbine" + new Date().getTime() + ".csv", {type: "text/plain;charset=utf-8"});
    saveAs(file);
}

var user;
$(function () {
    if (!$.cookie('user')) {
        location.replace('index.html');
    } else {
        try {
            user = JSON.parse($.cookie('user'));
        } catch (e) {
            $.removeCookie('user', {path: '/'});
            location.replace('index.html');
        }
        if (!user.id) {
            $.removeCookie('user', {path: '/'});
            location.replace('index.html');
        }
    }
    $('#logout').on('click', function () {
        $.removeCookie('user', {path: '/'});
        location.replace('index.html');
    });
    $("#send").on("click", sendSet);
    $("#save").on("click", sendListSettingsUser);
});

